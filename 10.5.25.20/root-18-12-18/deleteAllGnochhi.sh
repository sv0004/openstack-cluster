#!/usr/bin/bash
filename="$1"

while read -r line
do
    echo "deleting resource " 
	echo $line
	gnocchi resource delete $line 
done < "$filename"
