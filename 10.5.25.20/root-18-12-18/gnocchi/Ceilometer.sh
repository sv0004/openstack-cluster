yum install openstack-ceilometer-notification \
  openstack-ceilometer-central

cp /etc/ceilometer/ceilometer.conf /etc/ceilometer/ceilometer.conf.org
cp ceilometer.conf /etc/ceilometer/ceilometer.conf

ceilometer-upgrade --skip-metering-database

systemctl enable openstack-ceilometer-notification.service 
systemctl enable openstack-ceilometer-central.service
systemctl start openstack-ceilometer-notification.service 
systemctl start openstack-ceilometer-central.service
