yum install openstack-gnocchi-api openstack-gnocchi-metricd \
  python-gnocchiclient
cp /etc/gnocchi/gnocchi.conf /etc/gnocchi/gnocchi.conf.org
cp gnocchi.conf /etc/gnocchi/gnocchi.conf

cp api_audit_map.conf /etc/gnocchi/api_audit_map.conf

gnocchi-upgrade

systemctl enable openstack-gnocchi-api.service 
systemctl enable openstack-gnocchi-metricd.service
systemctl start openstack-gnocchi-api.service 
systemctl start openstack-gnocchi-metricd.service
