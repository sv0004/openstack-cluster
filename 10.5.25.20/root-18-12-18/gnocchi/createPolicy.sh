gnocchi archive-policy create \
    -b 0 -m min -m mean -m max -m count \
    -d 'points:5,granularity:30' \
    -d 'points:3,timespan:900' \
    -d 'timespan:3600,granularity:900' \
    -d 'points:24,granularity:3600' \
    my-low
