systemctl restart httpd
systemctl stop openstack-ceilometer-notification.service   openstack-ceilometer-central.service 
systemctl stop openstack-ceilometer-compute 
systemctl stop openstack-gnocchi-api.service  openstack-gnocchi-metricd.service
gnocchi-upgrade
ceilometer-upgrade --skip-metering-database
systemctl start openstack-gnocchi-api.service  openstack-gnocchi-metricd.service
systemctl start openstack-ceilometer-notification.service   openstack-ceilometer-central.service 
systemctl start openstack-ceilometer-compute 
