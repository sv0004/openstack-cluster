import json
import sys
from time import sleep
import requests
from requests import Request, Session

# ========== SET USER VARIABLES ========== #
System = "Cluster"  # AIO or MN
vm_deploy_count = int(sys.argv[1])  # number of VMs to deploy in stress test
metric = 'cpu_util'
sleep_timer = 80  # allows gnocchi time to gather the first metric

# ========== SET VM INFORMATION ========== #
if System == "AIO":
    keystone_ip = "10.5.24.27"
    gnocchi_ip = "10.5.24.27"
    image_ref = "962ecd3c-84b8-4d53-9130-dfcc0d898a33"  # cirros 040
    flavor_ref = "0"  # nano
    network_id = "a62507a2-0ce5-43c1-b793-781fe64a1a9a"  # admin-internal
    admin_password = "password"  # admin tenant password

elif System == "Cluster":
    keystone_ip = "10.5.24.31"
    gnocchi_ip = "10.5.24.29"
    image_ref = "d8a20f90-cdd9-4e46-a5ab-f374bde51f7d"  # cirros 040
    flavor_ref = "3337b729-a29d-461a-9211-aa3030f30141"  # nano
    network_id = "5dea8b72-4e49-47ff-9ee7-533e41d172b2"  # admin-private
    admin_password = "ADMIN_PASS"  # admin tenant password


def get_token_v3():
    headers = {
        'Content-Type': 'application/json'
    }

    body = {
        "auth": {
            "identity": {
                "methods": [
                    "password"
                ],
                "password": {
                    "user": {
                        "name": "admin",
                        "password": admin_password,
                        "domain": {
                                "id": "default"
                        }
                    }
                }
            },
            "scope": {
                "project": {
                    "name": "admin",
                    "domain": {"id": "default"}
                }
            }
        }
    }

    client_url = "http://" + keystone_ip + ":5000/v3/auth/tokens"

    r = requests.post(client_url, data=json.dumps(body), headers=headers)
    keystone_token = r.headers['X-Subject-Token']
    response_code = r.status_code

    if keystone_token and response_code == 201:
        print("~~~ GOT Gnocchi Token V3")
        print(keystone_token)
        return keystone_token
    else:
        print("~~~ DID NOT GET Gnocchi Token V3")
        print(response_code)
        exit(-1)


def gnocchi_request(auth_token, req_type, endpoint, query=None, body=None):
    s = Session()
    base_uri = "http://" + gnocchi_ip + ":8041/v1"
    client_url = base_uri + endpoint

    headers = {
        'X-Auth-Token': auth_token,
        'Content-Type': 'application/json',
    }

    try:
        req = Request(req_type, client_url, data=json.dumps(
            body), headers=headers, params=query)
        prepared_request = s.prepare_request(req)
        resp = s.send(prepared_request)
        return resp

    except Exception:
        print(" ~~~ Error Getting Response From Gnocchi at URL: " +
              client_url + " ~~~")
        exit(-1)


# create VM instance
def create_openstack_vm(auth_token):
    s = Session()

    headers = {
        'X-Auth-Token': auth_token,
        'Content-Type': 'application/json',
    }

    name = "Stress-Test-VM"

    body = {
        "server": {
            "name": name,
            "imageRef": image_ref,
            "flavorRef": flavor_ref,
            "networks": [
                {
                    "uuid": network_id
                }]
        }
    }

    client_url = "http://" + keystone_ip + ":8774/v2.1/servers"

    try:
        req = Request('POST', client_url,
                      data=json.dumps(body), headers=headers)
        prepared_request = s.prepare_request(req)
        resp = s.send(prepared_request)
        response_json = resp.json()
        vm_id = response_json['server']['id']
        print("~~~ Created VM With ID: " + vm_id)
        return vm_id

    except Exception:
        print("ERROR Creating VM")
        print("~~~ ERROR URL: " + client_url)
        print(resp.json())
        exit(-1)


def delete_openstack_vms(auth_token, vm_id_list):
    for delete_id in vm_id_list:

        s = Session()

        headers = {
            'X-Auth-Token': auth_token,
            'Content-Type': 'application/json',
        }

        client_url = "http://" + keystone_ip + ":8774/v2.1/servers/" + delete_id

        try:
            req = Request('DELETE', client_url, headers=headers)
            prepared_request = s.prepare_request(req)
            resp = s.send(prepared_request)
            delete_code = resp.status_code
            if delete_code != 204:
                print("ERROR Deleting VM - ID: " + delete_id)
                print("~~~ DELETE ERROR CODE: " + delete_code)
                exit(-1)

        except Exception:
            print(" ~~~ Error Deleting VM Instance at URL: " + client_url)
            print("~~~ Check OpenStack To Delete Error VM")
            exit(-1)
    return True


def gather_metrics(auth_token, vm_id):

    instance_response = gnocchi_request(
        auth_token, 'GET', '/resource/instance/' + vm_id)
    instance_response_code = instance_response.status_code

    if instance_response_code == 200 or instance_response_code == 201:

        # if we get the instance correctly, get the metric values of specified metric
        instance = instance_response.json()
        instance_metrics = instance['metrics']
        metric_id = instance_metrics[metric]

        metric_response = gnocchi_request(
            key_auth_token, 'GET', '/aggregation/' + metric_id + '/metric')
        metric_response_code = metric_response.status_code

        if metric_response_code == 200 or metric_response_code == 201:
            # this is the full array of readings for specific metric
            metric_response_array = metric_response.json()

            if len(metric_response_array) > 0:
                # only displays the last reading
                last_metric = metric_response_array[0]
                return last_metric
            else:
                return_message = "Metric Array Is Empty - gnocchi may need more time to gather first metric"
                return return_message
        else:
            print("~~~ Could Not Get Metric For VM: " + vm_id)
            exit(-1)
    else:
        print("~~~ METRIC GATHER ERROR")
        print(instance_response_code)
        exit(-1)


# internal variables
deployed_vm_ids = []
count = 0
key_auth_token = get_token_v3()
try:
    # create VMs
    while count < vm_deploy_count:
        created_vm_id = create_openstack_vm(key_auth_token)
        deployed_vm_ids.append(created_vm_id)
        count += 1

    print("~~~ ALL VMs Created")
    print("~~~ Starting Metric Gathering")

    print("waiting for a bit for gnocchi to gather at least 1 metric...")
    print("~~~ Wait Count Currently Set To:", sleep_timer)
    # see if this can be lowered -> change granularity in gnocchi?
    sleep(sleep_timer)

    # get metrics of VMs
    for deployed_id in deployed_vm_ids:
        metric_reading = gather_metrics(key_auth_token, deployed_id)
        print("~~~ METRIC READING for VM: " + deployed_id)
        print(metric_reading)

    print("~~~ ALL Metrics Gathered")
    print("~~~ Deleting VMs")

    # delete VMs
    delete_success = delete_openstack_vms(key_auth_token, deployed_vm_ids)
    if delete_success:
        print("~~~ TEST SUCCESSFUL")
    else:
        print("~~~ DELETE VMs ERROR -> Check Openstack For Running Test VMs")

# if the script fails, delete the VMs
except Exception:
    delete_success = delete_openstack_vms(key_auth_token, deployed_vm_ids)
    print("~~~ SCRIPT ERROR -> DELETING VMS")
    print("~~~ DELETE SUCCESS = ", delete_success)
