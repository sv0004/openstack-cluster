if [ -f controller.txt ]; then
        for server in $(cat controller.txt); do
                ssh -t root@$server -i ~/.ssh/openstack_rsa '
                echo $(export OS_USERNAME=admin)
                echo $(export OS_PASSWORD=ADMIN_PASS)
                echo $(export OS_PROJECT_NAME=admin)
                echo $(export OS_USER_DOMAIN_NAME=Default)
                echo $(export OS_PROJECT_DOMAIN_NAME=Default)
                echo $(export OS_AUTH_URL=http://10.5.25.20:35357/v3)
                echo $(export OS_IDENTITY_API_VERSION=3)
                echo $(export OS_IMAGE_API_VERSION=2)
                echo $(export OS_AUTH_TYPE=password)
                echo $(export OS_TENANT_NAME=admin)
                echo $(openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public)
                echo $(openstack subnet create --no-dhcp --subnet-range 10.5.25.0/24 --network public --allocation-pool=start=10.5.25.151,end=10.5.25.180 --gateway=10.5.25.10 public-subnet)
                echo $(openstack network create --no-share --internal admin-private)
                echo $(openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8)
                echo $(openstack router create admin-router)
                echo $(openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.25.151 admin-router)
                echo $(openstack router add subnet admin-router admin-private-subnet)        '
        done
else
        echo 'No server.txt file'
fi







