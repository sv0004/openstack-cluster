openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
openstack router create admin-router
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.24.130 admin-router
openstack router add subnet admin-router admin-private-subnet

