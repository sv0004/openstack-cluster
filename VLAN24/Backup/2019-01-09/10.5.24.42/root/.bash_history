yum update
ls
visudo
visudo
yum install vim
yum -y update
vi /etc/hostname 
reboot
exit
sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux
sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

clear
# Update the host
sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux
# Configure Host Files for DNS
sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

# Configure NTP
yum install chrony -y
sudo rm /etc/chrony.conf
sudo cat <<EOT>> /etc/chrony.conf
server vlan24-os-controller iburst


driftfile /var/lib/chrony/drift

makestep 1.0 3

rtcsync
EOT

systemctl enable chronyd.service
systemctl start chronyd.service
chronyc sources
vi /etc/chrony.conf 

chronyc sources
ping vlan24-os-controller
chronyc sources
systemctl stop firewalld
systemctl disable firewalld
chronyc sources
vi /etc/chrony.conf 
chronyc sources
reboot 
chronyc sources
df -h
clear
# install OpenStack Queens Packages
yum install centos-release-openstack-queens -y
yum upgrade -y
yum install python-openstackclient -y
yum install openstack-selinux -y
yum install crudini -y
# Stop and disable firewall
systemctl stop firewalld
systemctl disable firewalld
# Create Openstack Admin File
sudo rm /root/admin_rc
sudo cat <<EOT>> /root/admin_rc
export OS_USERNAME=admin
export OS_PASSWORD=ADMIN_PASS
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

sudo rm /root/demo_rc
sudo cat <<EOT>> /root/demo_rc
export OS_USERNAME=demo
export OS_PASSWORD=demo
export OS_PROJECT_NAME=demo
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

ls
reboot
vi /etc/lvm/lvm.conf 
pvs -v
df -h
vi /etc/lvm/lvm.conf 
pvs -v
vi /etc/lvm/lvm.conf 
pvs -v
sudo yum install -y openstack-nova-compute
sudo yum install -y openstack-neutron openstack-neutron-ml2 openstack-neutron-openvswitch
sudo yum install -y openstack-ceilometer-compute
sudo yum install -y python-networking-odl
ip a
clear
sudo rm /etc/nova/nova.conf
sudo cat <<EOT>> /etc/nova/nova.conf
[DEFAULT]
use_neutron = True
firewall_driver = nova.virt.firewall.NoopFirewallDriver
my_ip = 10.5.24.42
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
linuxnet_interface_driver = nova.network.linux_net.LinuxOVSInterfaceDriver
vif_plugging_is_fatal = True
vif_plugging_timeout = 300
instance_usage_audit = True
instance_usage_audit_period = hour
notify_on_state_change = vm_and_task_state
enabled_apis=osapi_compute,metadata
[api]
auth_strategy=keystone
[api_database]
[barbican]
[cache]
[cells]
[cinder]
os_region_name = RegionOne
[compute]
[conductor]
[console]
[consoleauth]
[cors]
[crypto]
[database]
[devices]
[ephemeral_storage_encryption]
[filter_scheduler]
[glance]
api_servers = http://10.5.24.31:9292
[guestfs]
[healthcheck]
[hyperv]
[ironic]
[key_manager]
[keystone]
[keystone_authtoken]
auth_uri = http://10.5.24.31:5000
auth_url = http://10.5.24.31:35357
memcached_servers = 10.5.24.31:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = nova
password = nova123
[libvirt]
[matchmaker_redis]
[metrics]
[mks]
[neutron]
auth_url = http://10.5.24.31:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = neutron123
service_metadata_proxy = True
metadata_proxy_shared_secret = metadata_secret
[notifications]
[osapi_v21]
[oslo_concurrency]
lock_path=/var/lib/nova/tmp
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
driver = messagingv2
[oslo_messaging_rabbit]
[oslo_messaging_zmq]
[oslo_middleware]
[oslo_policy]
policy_file=/etc/nova/policy.json
[pci]
[placement]
os_region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://10.5.24.31:35357/v3
username = placement
password = placement123
[quota]
[rdp]
[remote_debug]
[scheduler]
[serial_console]
[service_user]
[spice]
[upgrade_levels]
[vault]
[vendordata_dynamic_auth]
[vmware]
[vnc]
enabled = True
server_listen = 0.0.0.0
server_proxyclient_address = 10.5.24.42
novncproxy_base_url = http://10.5.24.31:6080/vnc_auto.html
[workarounds]
[wsgi]
[xenserver]
[xvp]
EOT

sudo rm /etc/neutron/neutron.conf
sudo cat <<EOT>> /etc/neutron/neutron.conf
[DEFAULT]
core_plugin = neutron.plugins.ml2.plugin.Ml2Plugin
service_plugins = odl-router_v2
auth_strategy = keystone
state_path = /var/lib/neutron
allow_overlapping_ips = True
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
[keystone_authtoken]
www_authenticate_uri = http://10.5.24.31:5000
auth_url = http://10.5.24.31:5000
memcached_servers = 10.5.24.31:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = neutron123
[oslo_concurrency]
lock_path = $state_path/lock
EOT

chmod 640 /etc/neutron/neutron.conf 
chgrp neutron /etc/neutron/neutron.conf 
cat /etc/hosts
clear
sudo rm /etc/neutron/dhcp_agent.ini
sudo cat <<EOT>> /etc/neutron/dhcp_agent.ini
[DEFAULT]
interface_driver = neutron.agent.linux.interface.OVSInterfaceDriver
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true
force_metadata = true
[agent]
[ovs]
ovsdb_connection = tcp:10.5.24.40:6640
EOT

clear
sudo rm /etc/neutron/metadata_agent.ini
sudo cat <<EOT>> /etc/neutron/metadata_agent.ini
[DEFAULT]
nova_metadata_host = 10.5.24.31
metadata_proxy_shared_secret = metadata_secret
[agent]
[cache]
memcache_servers = 10.5.24.31:11211
EOT

cat /etc/hosts
sudo rm /etc/neutron/plugins/ml2/ml2_conf.ini
sudo cat <<EOT>> /etc/neutron/plugins/ml2/ml2_conf.ini
[DEFAULT]
[l2pop]
[ml2]
type_drivers = flat,vlan,gre,vxlan
tenant_network_types = vxlan
mechanism_drivers = opendaylight_v2
extension_drivers = port_security
[ml2_type_flat]
flat_networks = extnet
[ml2_type_geneve]
[ml2_type_gre]
[ml2_type_vlan]
[ml2_type_vxlan]
vni_ranges = 1:1000
[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_security_group = true
enable_ipset = true
[ml2_odl]
url = http://10.5.24.40:8181/controller/nb/v2/neutron
password = admin
username = admin
EOT

clear
ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
clear
ls
. admin_rc 
systemctl enable libvirtd.service openstack-nova-compute.service
systemctl start libvirtd.service openstack-nova-compute.service
systemctl enable openvswitch
systemctl start openvswitch
systemctl
clear
ovs-vsctl show
cat /etc/hosts
ovs-vsctl set-manager tcp:10.5.24.40:6640
ip a
clear
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
clear
for service in dhcp-agent metadata-agent; do systemctl start neutron-$service; systemctl enable neutron-$service; done
cd /etc/ceilometer/
ls
sudo rm /etc/ceilometer/ceilometer.coni
sudo cat <<EOT>> /etc/ceilometer/ceilometer.conf
[DEFAULT]
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
[compute]
[coordination]
[dispatcher_gnocchi]
[event]
[hardware]
[ipmi]
[matchmaker_redis]
[meter]
[notification]
[oslo_concurrency]
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_messaging_zmq]
[polling]
[publisher]
[publisher_notifier]
[rgw_admin_credentials]
[service_credentials]
auth_url = http://10.5.24.31:5000
project_domain_id = default
user_domain_id = default
auth_type = password
username = ceilometer
project_name = service
password = ceilometer123
interface = internalURL
region_name = RegionOne
[service_types]
[vmware]
[xenapi]
EOT

clear
sudo rm /etc/ceilometer/pipeline.yaml
sudo cat <<EOT>> /etc/ceilometer/pipeline.yaml
---
sources:
    - name: meter_source
      meters:
          - "*"
      sinks:
          - meter_sink
    - name: cpu_source
      meters:
          - "cpu"
      sinks:
          - cpu_sink
          - cpu_delta_sink
    - name: disk_source
      meters:
          - "disk.read.bytes"
          - "disk.read.requests"
          - "disk.write.bytes"
          - "disk.write.requests"
          - "disk.device.read.bytes"
          - "disk.device.read.requests"
          - "disk.device.write.bytes"
          - "disk.device.write.requests"
      sinks:
          - disk_sink
    - name: network_source
      meters:
          - "network.incoming.bytes"
          - "network.incoming.packets"
          - "network.outgoing.bytes"
          - "network.outgoing.packets"
      sinks:
          - network_sink
sinks:
    - name: meter_sink
      transformers:
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: cpu_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                target:
                    name: "cpu_util"
                    unit: "%"
                    type: "gauge"
                    max: 100
                    scale: "100.0 / (10**9 * (resource_metadata.cpu_number or 1))"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: cpu_delta_sink
      transformers:
          - name: "delta"
            parameters:
                target:
                    name: "cpu.delta"
                growth_only: True
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: disk_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                    map_from:
                        name: "(disk\\.device|disk)\\.(read|write)\\.(bytes|requests)"
                        unit: "(B|request)"
                target:
                    map_to:
                        name: "\\1.\\2.\\3.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: network_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                   map_from:
                       name: "network\\.(incoming|outgoing)\\.(bytes|packets)"
                       unit: "(B|packet)"
                target:
                    map_to:
                        name: "network.\\1.\\2.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high

EOT

clear
sudo rm /etc/ceilometer/polling.yaml
sudo cat <<EOT>> /etc/ceilometer/polling.yaml
---
sources:
    - name: some_pollsters
      interval: 5
      meters:
        - cpu
        - cpu_l3_cache
        - memory.usage
        - network.incoming.bytes
        - network.incoming.packets
        - network.outgoing.bytes
        - network.outgoing.packets
        - disk.device.read.bytes
        - disk.device.read.requests
        - disk.device.write.bytes
        - disk.device.write.requests
        - hardware.cpu.util
        - hardware.memory.used
        - hardware.memory.total
        - hardware.memory.buffer
        - hardware.memory.cached
        - hardware.memory.swap.avail
        - hardware.memory.swap.total
        - hardware.system_stats.io.outgoing.blocks
        - hardware.system_stats.io.incoming.blocks
        - hardware.network.ip.incoming.datagrams
        - hardware.network.ip.outgoing.datagrams
EOT

systemctl enable openstack-ceilometer-compute.service
systemctl start openstack-ceilometer-compute.service
systemctl restart openstack-nova-compute.service
shutdown -r now
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl show
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
reboot
systemctl firewalld stop
systemctl stop firewalld
systemctl disable firewalld
ip a
cat /etc/hosts
ovs-vsctl show
systemctl is-enabled firewalld
systemctl is-active firewalld
reboot
ovs-vsctl show
ls
ls /var/log/openvswitch/
echo $(systemctl stop openvswitch)
vswith/*)
vswitch/conf.db)
systemctl stop openvswitch
rm -rf /var/log/openvswitch/*
rm -rf /etc/openvswitch/conf.db 
ls /var/log/openvswitch/
ip q
ip a
cat /etc/hosts
systemctl start openvswitch
ovs-vsctl show
ovs-vsctl set-manager tcp:10.5.24.40:6640
ovs-vsctl show
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl show
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
shutdown -r now
ovs-vsctl show
ovs-vsctl del-manager 6640:10.5.24.40
ovs-vsctl del-manager --help
ovs-vsctl del-manager
ovs-vsctl show
ovs-vsctl del-manager
ovs-vsctl show
ovs-vsctl set-manager ptcp:127.0.0.1
ovs-vsctl show
ovs-vsctl set-manager ptcp:6640:127.0.0.1
ovs-vsctl show
ovs-vsctl del-manager
ovs-vsctl show
ovs-vsctl --help
vsc-vsctl show
ovs-vsctl show
ovs-vsctl set-manager tcp:10.5.24.40:6640
ovs-vsctl set-manager ptcp:127.0.0.1:6640
ovs-vsctl show
systemctl stop openvswitch
rm -rf /var/log/openvswitch/*
rm -rf /etc/openvswitch/conf.db 
exit
ovs-vsctl show
ovs-vsctl set-manager ptcp:6640
ovs-vsctl show
ovs-vsctl --help
ovs-vsctl show
ovs-vsctl remove set-manager ptcp:6640:10.5.24.20
ovs-vsctl --help
ovs-vsctl show
systemctl start openvswitch
ovs-vsctl set-manager ptcp:6640
ovs-vsctl set-manager tcp:10.5.24.40:6640
ovs-vsctl show
ovs-vsctl set-manager ptcp:127.0.0.1:6640
ovs-vsctl show
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl show
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
shutdown -r now
ovs-vsctl show
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private)
openstack network create --no-share --internal admin-private
. admin_rc 
ovs-vsctl show
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vsctl show
cd /etc/neutron/
cd plugins
ls
cd ..
ls
pwd
cd plugins/
ls
vi dhcp_agent.ini
ls
vi neutron.conf
ls
cd ml2/
ls
vi ml2_conf.ini 
ls
vi ml2_conf_odl.ini 
ls
vi openvswitch_agent.ini 
vi sriov_agent.ini 
clear
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
ip a
cat /etc/hosts
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
hutdown -r now
shutdown -r now
ovs-vsctl shoq
ovs-vsctl show
ls
. admin_rc 
ls
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network list
openstack subnet list
ovs-vsctl show
openstack network create --no-share --internal admin-private
ovs-vsctl show
ip a
ovs-vsctl show
cat /etc/hosts
ovs-vsctl show
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
ovs-vsctl show
sudo ovs-vsctl set-manager ptcp:6640
ovs-vsctl show
ovs-vsctl del-manager
ovs-vsctl show
sudo ovs-vsctl set-manager ptcp:6640
ovs-vsctl show
ovs-vsctl del-manager --help
curl http://10.5.24.40:8181/restconf/config/network-topology:network-topology/topology/ovsdb:1/
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl show
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl show
cat /etc/hosts
cd /etc/neutron/
cd plugins
ls
vi neutron.conf 
ls
cd ml2/
ls
vi ml2_conf.ini 
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl openvswitch restart
systemctl restart openvswitch
ovs-vsctl show
cd
.a demo_rc 
. admin_rc 
ls
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
ovs-vsctl show
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
ovs-vsctl show
openstack router create admin-router
ovs-vsctl show
openstack network create --no-share --internal admin-private
ovs-vsctl show
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
ovs-vsctl show
curl -u admin:admin http://10.5.24.40:8080/controller/nb/v2/neutron/networks
curl -u admin:admin http://10.5.24.40:8181/controller/nb/v2/neutron/networks
penstack router list
openstack router list
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.25.151 admin-router
openstack router add subnet admin-router admin-private-subnet
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.24.151 admin-router
openstack router add subnet admin-router admin-private-subnet
cat /etc/hosts
vi /etc/neutron/plugins/ml2/ml2_conf.ini 
cd /etc/
cd neutron/
ls
exit
ls
cd /etc/neutron/
;s
ls
vi neutron.conf 
cd plugins
ls
cat dhcp_agent.ini 
cat neutron.conf 
cd ml2
ls
vi ml2_conf.ini 
cat openvswitch_agent.ini 
vi openvswitch_agent.ini 
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
exit
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
ovs-vsctl show
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch
ovs-vsctl show
shutdown -r now
. admin_rc 
ovs-vsctl show
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
ovs-vsctl show
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
ovs-vsctl show
openstack router create admin-router
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.24.151 admin-router
openstack router add subnet admin-router admin-private-subnet
vi /etc/neutron/dhcp_agent.ini 
ls
vi /etc/neutron/metadata_agent.ini 
vi /etc/neutron/plugins/ml2/ml2_conf.ini 
cd /etc/neutron/
cd /var/log/neutron/
ls
cat dhcp-agent.log
ls
cat metadata-agent.log 
ls -al
rm prf *
ls
cd /var/log
ls
cd openvswitch/
ls
cat ovs-vswitchd.log
ls
cay ovsdb-server.log
cat ovsdb-server.log
ip a
clear
curl -s -u admin:admin -X GET http://10.5.24.31:8181/restconf/operational/neutron:neutron/hostconfigs/
curl -s -u admin:admin -X GET http://10.5.24.31:8181/restconf/operational/neutron:neutron/hostconfigs/ | python -m json.tool
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
curl -s -u admin:admin -X GET http://10.5.24.31:8181/restconf/operational/neutron:neutron/hostconfigs/ | python -m json.tool
curl -s -u admin:admin -X GET http://10.5.24.31:8181/restconf/operational/neutron:neutron/hostconfigs/
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vsctl show
ovs-vsctl del-manager
ovs-vsctl show
ovs-vsctl del-manager
ovs-vsctl show
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vsctl show
curl -s -u admin:admin -X GET http://10.5.24.31:8181/restconf/operational/neutron:neutron/hostconfigs/
systemctl stop neutron-openvswitch-agent
systemctl disable
systemctl disable meutorn-openvswitch-agent
systemctl stop neutron-l3-agent
systemctl disable neutron-l3-agent
systemctl stop openvswitch
rm -rf /var/log/openvswitch/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vsctl show
cd
. admin_rc 
ls
ovs-vsctl show
shutdown -r now
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vscl show
ovs-vsctl show
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
cd
. ad
. admin_rc 
ls
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
ovs-vsctl show
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
openstack router create admin-router
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.24.151 admin-router
openstack router add subnet admin-router admin-private-subnet
ip a
cd /var/log/openvswitch/
ls
cat ovsdb-server.log 
cd /etc/openvswitch/
ls
cat default.conf 
cat system-id.conf 
cat conf.db 
vi conf.db
ls
cat conf.db
vi conf.db 
cleat
clear
cd /etc
grep -lr 10.5.24.40 *
find /etc -type f | xargs grep -l 10.5.24.40
vi /etc/neutron/dhcp_agent.ini 
ovs-vsctl show
systemctl stop openvswitch
rm -rf /var/log/openvswith/*
rm -rf /etc/openvswitch/conf.db
ls /etc/openvswitch/
shutdown -r now
ovs-vsctl show
systemctl start openvswitch
ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
ovs-vsctl show
cd
. admin_rc 
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public
openstack subnet create --no-dhcp --subnet-range 10.5.24.0/24 --network public --allocation-pool=start=10.5.24.130,end=10.5.24.170 --gateway=10.5.24.10 public-subnet
openstack network create --no-share --internal admin-private
openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8
openstack router create admin-router
openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.24.151 admin-router
openstack router add subnet admin-router admin-private-subnet
ovs-vsctl show
ping 10.5.24.139
cat /etc/hosts
ssh root@10.5.24.28
exit
ls
mv /etc/ceilometer/ceilometer.conf /etc/ceilometer/ceilometer.conf.org
vi /etc/ceilometer/ceilometer.conf 
640 /etc/ceilometer/ceilometer.conf
chmod 640 /etc/ceilometer/ceilometer.conf
chgrp ceilometer /etc/ceilometer/ceilometer.conf 
systemctl restart openstack-ceilometer-compute 
systemctl start openstack-ceilometer-compute 
systemctl
cd /var/log/ceilometer/
vi /etc/ceilometer/ceilometer.conf
systemctl restart openstack-ceilometer-compute 
cd /var/log/ceilometer/
ls
tail -f compute.log 
exit
ls -al
systemctl disable firewalld
systemctl stop firewalld
systemctl disable NetworkManager
systemctl stop NetworkManager
systemctl enable network
systemctl start network
sestatus
cd /var/log/ceilometer/
ls
tail -f compute.log
cd /etc/ceilometer/
ls
vi ceilometer.conf
exit
ls -al
cd ..
exit
ip a
cat ,ssh
exit
ip a
cd /etc/sysconfig/network-scripts/
ls
vi ifcfg-em2
ifup em2
ip a
reboot
ip a
ping 10.5.24.137
ping 10.5.24.134
ssh cirros@10.5.24.134
exit
. admin_rc 
ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=211, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:212"
ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=241, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:242"
exit
cd /etc/nova/
ls
cat policy.json 
scp root@10.5.25.31/etc/nova/policy.json policy.json
scp root@10.5.25.31:/etc/nova/policy.json policy.json
exit
ls
cd /etc/nova/
ls
cat policy.json 
reboot
df -h
