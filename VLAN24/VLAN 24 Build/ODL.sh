# Install base CentOS7 on the controller

# Configure hostsnames
sudo rm /etc/hostname
sudo cat <<EOT>> /etc/hostname
vlan24-os-ODL
EOT

# Update the host

sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux
sudo yum -y install wget

# Configure Host Files for DNS

sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

# Configure NTP
yum install chrony -y

sudo rm /etc/chrony.conf
sudo cat <<EOT>> /etc/chrony.conf
server vlan24-os-controller iburst


# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates
# if its offset is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync
EOT


systemctl enable chronyd.service
systemctl restart chronyd.service

# configure OLD

yum install -y java-1.8.0-openjdk.x86_64

export FEATURES=odl-netvirt-openstack,odl-dlux-core,odl-mdsal-apidocs,odl-dluxapps-nodes,odl-dluxapps-yangutils,odl-dluxapps-topology,odl-dluxapps-applications,odl-dluxapps-yangman,odl-dluxapps-yangui
export JAVA_HOME=/usr/lib/jvm/jre
rm -rf /root/karaf-0.8.2
tar -C /root -xvf /root/karaf-0.8.2.tar.gz
sed -i $ep '/^featuresBoot = / s/$/,'$FEATURES'/' /root/karaf-0.8.2/etc/org.apache.karaf.features.cfg
/root/karaf-0.8.2/bin/start clean
sleep 6m
