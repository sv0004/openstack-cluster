
if [ -f /root/scripts/compute.txt ]; then
        for server in $(cat /root/scripts/compute.txt); do
                ssh -t root@$server -i /root/.ssh/openstack_rsa '
                echo $(ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=211, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:212")
                echo $(ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=241, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:242")                '
        done
else
        echo 'No server.txt file'
fi

if [ -f /root/scripts/controller.txt ]; then
        for server in $(cat /root/scripts/controller.txt); do
                ssh -t root@$server -i /root/.ssh/openstack_rsa '
                echo $(export OS_USERNAME=admin)
                echo $(export OS_PASSWORD=ADMIN_PASS)
                echo $(export OS_PROJECT_NAME=admin)
                echo $(export OS_USER_DOMAIN_NAME=Default)
                echo $(export OS_PROJECT_DOMAIN_NAME=Default)
                echo $(export OS_AUTH_URL=http://10.5.25.20:35357/v3)
                echo $(export OS_IDENTITY_API_VERSION=3)
                echo $(export OS_IMAGE_API_VERSION=2)
                echo $(export OS_AUTH_TYPE=password)
                echo $(export OS_TENANT_NAME=admin)
                echo $(openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public)
                echo $(openstack subnet create --no-dhcp --subnet-range 10.5.25.0/24 --network public --allocation-pool=start=10.5.25.151,end=10.5.25.180 --gateway=10.5.25.10 public-subnet)
                echo $(openstack network create --no-share --internal admin-private)
                echo $(openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8)
                echo $(openstack router create admin-router)
                echo $(openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.25.151 admin-router)
                echo $(openstack router add subnet admin-router admin-private-subnet) 
                echo $(openstack security group create --description '5GIC Default Security Group' --project admin 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol icmp --ingress 5GIC) 
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol icmp --egress 5GIC) 
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol 132 --egress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol 132 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol TCP --dst-port 22 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol TCP --dst-port 3000 --ingress 5GIC)

                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45363 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45363 --egress 5GIC)

                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45362 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45362 --egress 5GIC)
                
                       '
        done
else
        echo 'No server.txt file'
fi







