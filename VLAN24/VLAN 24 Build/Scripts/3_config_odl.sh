export FEATURES=odl-netvirt-openstack,odl-dlux-core,odl-mdsal-apidocs,odl-dluxapps-nodes,odl-dluxapps-topology,odl-dluxapps-applications,odl-dluxapps-yangman,odl-dluxapps-yangui
export JAVA_HOME=/usr/lib/jvm/jre
rm -rf /root/karaf-0.8.2
tar -C /root -xvf /root/karaf-0.8.2.tar.gz
sed -i $ep '/^featuresBoot = / s/$/,'$FEATURES'/' /root/karaf-0.8.2/etc/org.apache.karaf.features.cfg
/root/karaf-0.8.2/bin/start clean
sleep 6m
