#!/bin/bash
rm /tmp/5gic-recover.log
touch /tmp/5gic-recover.log
# Check that all cluster hosts are up and running

if [ -f /root/scripts/hosts.txt ]; then
        for server in $(cat /root/scripts/hosts.txt); do
                printf "%s" "waiting for $server ..."
                while ! timeout 0.2 ping -c 1 -n $server &> /dev/null
                do
                        printf "%c" "."
                done
                printf "\n%s\n"  "$server is back online"
        done
        echo 'All hosts online'
else
        echo 'No hosts.txt file'
fi

# Stop Openvswitch on all compute servers

if [ -f /root/scripts/compute.txt ]; then
        for server in $(cat /root/scripts/compute.txt); do
            ssh -t root@$server -i /root/.ssh/openstack_rsa '
            echo $(systemctl stop openvswitch)
		    echo $(rm -rf /var/log/openvswith/*)
		    echo $(rm -rf /etc/openvswitch/conf.db)                '
        done
else
        echo 'No server.txt file'
fi

# Reset Neutron Database

if [ -f /root/scripts/controller.txt ]; then
        for server in $(cat /root/scripts/controller.txt); do
            ssh -t root@$server -i /root/.ssh/openstack_rsa '
		    echo $(systemctl stop neutron-server)
            echo $(mysql -uroot -pabc.123 -e "DROP DATABASE neutron")
            echo $(mysql -uroot -pabc.123 -e "CREATE DATABASE neutron")
		    echo $(su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron)               '
        done
else
        echo 'No server.txt file'
fi

# Configure ODL Server

export FEATURES=odl-netvirt-openstack,odl-dlux-core,odl-mdsal-apidocs,odl-dluxapps-nodes,odl-dluxapps-topology,odl-dluxapps-applications,odl-dluxapps-yangman,odl-dluxapps-yangui
export JAVA_HOME=/usr/lib/jvm/jre
rm -rf /root/karaf-0.8.2
tar -C /root -xvf /root/karaf-0.8.2.tar.gz
sed -i $ep '/^featuresBoot = / s/$/,'$FEATURES'/' /root/karaf-0.8.2/etc/org.apache.karaf.features.cfg
/root/karaf-0.8.2/bin/start clean
sleep 6m

# Configure OVS on compute nodes

ssh -t root@10.5.24.42 -i /root/.ssh/openstack_rsa '
echo $(systemctl start openvswitch)
echo $(ovs-vsctl set-manager tcp:10.5.24.40:6640)
echo $(sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42)
echo $(sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2)
echo $(sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk)
echo $(sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk)
echo $(systemctl restart openvswitch)
echo $(ovs-vsctl show)
echo $(shutdown -r now) '

# wait for compute nodes to reboot
if [ -f /root/scripts/compute.txt ]; then
        for server in $(cat /root/scripts/compute.txt); do
                printf "%s" "waiting for $server ..."
                while ! timeout 0.2 ping -c 1 -n $server &> /dev/null
                do
                        printf "%c" "."
                done
                printf "\n%s\n"  "$server is back online"
        done
        echo 'All hosts online'
else
        echo 'No hosts.txt file'
fi

# restart neutron

if [ -f /root/scripts/controller.txt ]; then
        for server in $(cat /root/scripts/controller.txt); do
                ssh -t root@$server -i /root/.ssh/openstack_rsa '


                echo $(systemctl restart neutron-server)        '
        done
else
        echo 'No server.txt file'
fi

# Redeploy Network


if [ -f /root/scripts/compute.txt ]; then
        for server in $(cat /root/scripts/compute.txt); do
                ssh -t root@$server -i /root/.ssh/openstack_rsa '
                echo $(ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=211, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:212")
                echo $(ovs-ofctl add-flow br-int -O openflow13 "cookie=0x6900000, table=241, priority=100, dl_type=0x0800,nw_proto=132, actions=write_metadata:0/0x2,goto_table:242")                '
        done
else
        echo 'No server.txt file'
fi

if [ -f /root/scripts/controller.txt ]; then
        for server in $(cat /root/scripts/controller.txt); do
                ssh -t root@$server -i /root/.ssh/openstack_rsa '
                echo $(export OS_USERNAME=admin)
                echo $(export OS_PASSWORD=ADMIN_PASS)
                echo $(export OS_PROJECT_NAME=admin)
                echo $(export OS_USER_DOMAIN_NAME=Default)
                echo $(export OS_PROJECT_DOMAIN_NAME=Default)
                echo $(export OS_AUTH_URL=http://10.5.25.20:35357/v3)
                echo $(export OS_IDENTITY_API_VERSION=3)
                echo $(export OS_IMAGE_API_VERSION=2)
                echo $(export OS_AUTH_TYPE=password)
                echo $(export OS_TENANT_NAME=admin)
                echo $(openstack network create --share --external --provider-network-type flat --provider-physical-network extnet public)
                echo $(openstack subnet create --no-dhcp --subnet-range 10.5.25.0/24 --network public --allocation-pool=start=10.5.25.151,end=10.5.25.180 --gateway=10.5.25.10 public-subnet)
                echo $(openstack network create --no-share --internal admin-private)
                echo $(openstack subnet create --dhcp --subnet-range 10.0.0.0/24 --network admin-private admin-private-subnet  --dns-nameserver 8.8.8.8)
                echo $(openstack router create admin-router)
                echo $(openstack router set --external-gateway public --fixed-ip subnet=public-subnet,ip-address=10.5.25.151 admin-router)
                echo $(openstack router add subnet admin-router admin-private-subnet) 
                echo $(openstack security group create --description '5GIC Default Security Group' --project admin 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol icmp --ingress 5GIC) 
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol icmp --egress 5GIC) 
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol 132 --egress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol 132 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol TCP --dst-port 22 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol TCP --dst-port 3000 --ingress 5GIC)

                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45363 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45363 --egress 5GIC)

                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45362 --ingress 5GIC)
                echo $(openstack security group rule create --remote-ip 0.0.0.0/0 --protocol UDP --dst-port 45362 --egress 5GIC)
                
                       '
        done
else
        echo 'No server.txt file'
fi








