#!/bin/bash
if [ -f server.txt ]; then
        for server in $(cat server.txt); do
                ssh -t root@$server -i ~/.ssh/id_openstack '
                echo $(uname -r)                '
        done
else
        echo 'No server.txt file'
fi
