# Install base CentOS7 on the controller

# Configure hostsnames
sudo rm /etc/hostname
sudo cat <<EOT>> /etc/hostname
vlan24-os-block
EOT

# Update the host

sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux

# Configure Host Files for DNS

sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

# Configure NTP
yum install chrony -y

sudo rm /etc/chrony.conf
sudo cat <<EOT>> /etc/chrony.conf
server vlan24-os-controller iburst


# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates
# if its offset is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync
EOT


systemctl enable chronyd.service
systemctl restart chronyd.service

# install OpenStack Queens Packages
yum install centos-release-openstack-queens -y
yum upgrade -y
yum install python-openstackclient -y
yum install openstack-selinux -y
yum install crudini -y

# Stop and disable firewall
systemctl stop firewalld
systemctl disable firewalld




# Create Openstack Admin File
sudo rm /root/admin_rc
sudo cat <<EOT>> /root/admin_rc
export OS_USERNAME=admin
export OS_PASSWORD=ADMIN_PASS
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

sudo rm /root/demo_rc
sudo cat <<EOT>> /root/demo_rc
export OS_USERNAME=demo
export OS_PASSWORD=demo
export OS_PROJECT_NAME=demo
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT
 
#Install LVM service
yum install lvm2 device-mapper-persistent-data
systemctl enable lvm2-lvmetad.service
systemctl start lvm2-lvmetad.service

#Create the volumes. System has been configured with LVM on /dev/sda4
vgcreate cinder-volumes /dev/sda4

#add filter to /etc/lvm/lvm.conf (needs to be scripted)
devices {
...
filter = [ "a/sda4/", "r/.*/"]

# install openstack components.
yum install openstack-cinder targetcli python-keystone -y

# Configure Cinder
crudini --set  /etc/cinder/cinder.conf database connection "mysql+pymysql://cinder:CINDER_DBPASS@10.5.24.31/cinder"
crudini --set  /etc/cinder/cinder.conf DEFAULT transport_url "rabbit://openstack:RABBIT_PASS@10.5.24.31"
crudini --set  /etc/cinder/cinder.conf DEFAULT auth_strategy "keystone"

crudini --set  /etc/cinder/cinder.conf keystone_authtoken auth_uri "http://10.5.24.31:5000"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken auth_url "http://10.5.24.31:5000"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken memcached_servers "10.5.24.31:11211"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken auth_type "password"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken project_domain_id "default"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken user_domain_id "default"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken project_name "service"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken username "cinder"
crudini --set  /etc/cinder/cinder.conf keystone_authtoken password "cinder123"

crudini --set  /etc/cinder/cinder.conf DEFAULT my_ip "10.5.24.28"
crudini --set  /etc/cinder/cinder.conf DEFAULT enabled_backends "lvm"
crudini --set  /etc/cinder/cinder.conf DEFAULT glance_api_servers "http://10.5.24.31:9292"

crudini --set  /etc/cinder/cinder.conf lvm volume_driver "cinder.volume.drivers.lvm.LVMVolumeDriver"
crudini --set  /etc/cinder/cinder.conf lvm volume_group "cinder-volumes"
crudini --set  /etc/cinder/cinder.conf lvm iscsi_protocol "iscsi"
crudini --set  /etc/cinder/cinder.conf lvm iscsi_helper "lioadm"

crudini --set  /etc/cinder/cinder.conf oslo_concurrency lock_path "/var/lib/cinder/tmp"

systemctl enable openstack-cinder-volume.service target.service
systemctl start openstack-cinder-volume.service target.service





