# Install base CentOS7 on the controller

# Configure hostsnames
sudo rm /etc/hostname
sudo cat <<EOT>> /etc/hostname
vlan24-os-compute
EOT

# Update the host

sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux

# Configure Host Files for DNS

sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

# Configure NTP
yum install chrony -y

sudo rm /etc/chrony.conf
sudo cat <<EOT>> /etc/chrony.conf
server vlan24-os-controller iburst


# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates
# if its offset is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync
EOT


systemctl enable chronyd.service
systemctl start chronyd.service

# install OpenStack Queens Packages
yum install centos-release-openstack-queens -y
yum upgrade -y
yum install python-openstackclient -y
yum install openstack-selinux -y
yum install crudini -y

# Stop and disable firewall
systemctl stop firewalld
systemctl disable firewalld




# Create Openstack Admin File
sudo rm /root/admin_rc
sudo cat <<EOT>> /root/admin_rc
export OS_USERNAME=admin
export OS_PASSWORD=ADMIN_PASS
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

sudo rm /root/demo_rc
sudo cat <<EOT>> /root/demo_rc
export OS_USERNAME=demo
export OS_PASSWORD=demo
export OS_PROJECT_NAME=demo
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT



#Add Filter to LVM
[ "a/sda2/", "a/sda4/", "r/.*/"]

# install components

sudo yum install -y openstack-nova-compute
sudo yum install -y openstack-neutron openstack-neutron-ml2 openstack-neutron-openvswitch
sudo yum install -y openstack-ceilometer-compute
sudo yum install -y python-networking-odl

sudo rm /etc/nova/nova.conf
sudo cat <<EOT>> /etc/nova/nova.conf
[DEFAULT]
use_neutron = True
firewall_driver = nova.virt.firewall.NoopFirewallDriver
# be sure to change IP address
my_ip = 10.5.24.42
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
linuxnet_interface_driver = nova.network.linux_net.LinuxOVSInterfaceDriver
vif_plugging_is_fatal = True
vif_plugging_timeout = 300
instance_usage_audit = True
instance_usage_audit_period = hour
notify_on_state_change = vm_and_task_state
enabled_apis=osapi_compute,metadata
[api]
auth_strategy=keystone
[api_database]
[barbican]
[cache]
[cells]
[cinder]
os_region_name = RegionOne
[compute]
[conductor]
[console]
[consoleauth]
[cors]
[crypto]
[database]
[devices]
[ephemeral_storage_encryption]
[filter_scheduler]
[glance]
api_servers = http://10.5.24.31:9292
[guestfs]
[healthcheck]
[hyperv]
[ironic]
[key_manager]
[keystone]
[keystone_authtoken]
auth_uri = http://10.5.24.31:5000
auth_url = http://10.5.24.31:35357
memcached_servers = 10.5.24.31:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = nova
password = nova123
[libvirt]
[matchmaker_redis]
[metrics]
[mks]
[neutron]
auth_url = http://10.5.24.31:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = neutron123
service_metadata_proxy = True
metadata_proxy_shared_secret = metadata_secret
[notifications]
[osapi_v21]
[oslo_concurrency]
lock_path=/var/lib/nova/tmp
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
driver = messagingv2
[oslo_messaging_rabbit]
[oslo_messaging_zmq]
[oslo_middleware]
[oslo_policy]
policy_file=/etc/nova/policy.json
[pci]
[placement]
os_region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://10.5.24.31:35357/v3
username = placement
password = placement123
[quota]
[rdp]
[remote_debug]
[scheduler]
[serial_console]
[service_user]
[spice]
[upgrade_levels]
[vault]
[vendordata_dynamic_auth]
[vmware]
[vnc]
enabled = True
server_listen = 0.0.0.0
#change ip address
server_proxyclient_address = 10.5.24.42
novncproxy_base_url = http://10.5.24.31:6080/vnc_auto.html
[workarounds]
[wsgi]
[xenserver]
[xvp]
EOT



sudo rm /etc/neutron/neutron.conf
sudo cat <<EOT>> /etc/neutron/neutron.conf
[DEFAULT]
core_plugin = neutron.plugins.ml2.plugin.Ml2Plugin
service_plugins = odl-router_v2
auth_strategy = keystone
state_path = /var/lib/neutron
allow_overlapping_ips = True
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
[keystone_authtoken]
www_authenticate_uri = http://10.5.24.31:5000
auth_url = http://10.5.24.31:5000
memcached_servers = 10.5.24.31:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = neutron123
[oslo_concurrency]
lock_path = $state_path/lock
EOT

chmod 640 /etc/neutron/neutron.conf 
chgrp neutron /etc/neutron/neutron.conf 

sudo rm /etc/neutron/dhcp_agent.ini
sudo cat <<EOT>> /etc/neutron/dhcp_agent.ini
[DEFAULT]
interface_driver = neutron.agent.linux.interface.OVSInterfaceDriver
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true
force_metadata = true
[agent]
[ovs]
ovsdb_connection = tcp:10.5.24.40:6640
EOT

sudo rm /etc/neutron/metadata_agent.ini
sudo cat <<EOT>> /etc/neutron/metadata_agent.ini
[DEFAULT]
nova_metadata_host = 10.5.24.31
metadata_proxy_shared_secret = metadata_secret
[agent]
[cache]
memcache_servers = 10.5.24.31:11211
EOT

sudo rm /etc/neutron/plugins/ml2/ml2_conf.ini
sudo cat <<EOT>> /etc/neutron/plugins/ml2/ml2_conf.ini
[DEFAULT]
[l2pop]
[ml2]
type_drivers = flat,vlan,gre,vxlan
tenant_network_types = vxlan
mechanism_drivers = opendaylight_v2
extension_drivers = port_security
[ml2_type_flat]
flat_networks = extnet
[ml2_type_geneve]
[ml2_type_gre]
[ml2_type_vlan]
[ml2_type_vxlan]
vni_ranges = 1:1000
[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_security_group = true
enable_ipset = true
[ml2_odl]
url = http://10.5.24.40:8181/controller/nb/v2/neutron
password = admin
username = admin
EOT

ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini

systemctl enable libvirtd.service openstack-nova-compute.service
systemctl start libvirtd.service openstack-nova-compute.service
systemctl enable openvswitch
systemctl start openvswitch

ovs-vsctl set-manager tcp:10.5.24.40:6640
sudo ovs-vsctl set Open_vSwitch . other_config:local_ip=10.5.24.42
sudo ovs-vsctl set Open_vSwitch . other_config:provider_mappings=extnet:em2
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
sudo neutron-odl-ovs-hostconfig --local_ip 10.5.24.42 --host vlan24-os-compute --vhostuser_socket_dir /tmp --debug --datapath_type system  --bridge_mappings extnet:em2 --noovs_dpdk
systemctl restart openvswitch

for service in dhcp-agent metadata-agent; do
systemctl start neutron-$service
systemctl enable neutron-$service
done


sudo rm /etc/ceilometer/ceilometer.coni
sudo cat <<EOT>> /etc/ceilometer/ceilometer.conf
[DEFAULT]
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
[compute]
[coordination]
[dispatcher_gnocchi]
[event]
[hardware]
[ipmi]
[matchmaker_redis]
[meter]
[notification]
[oslo_concurrency]
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_messaging_zmq]
[polling]
[publisher]
[publisher_notifier]
[rgw_admin_credentials]
[service_credentials]
auth_url = http://10.5.24.31:5000
project_domain_id = default
user_domain_id = default
auth_type = password
username = ceilometer
project_name = service
password = ceilometer123
interface = internalURL
region_name = RegionOne
[service_types]
[vmware]
[xenapi]
EOT


sudo rm /etc/ceilometer/pipeline.yaml
sudo cat <<EOT>> /etc/ceilometer/pipeline.yaml
---
sources:
    - name: meter_source
      meters:
          - "*"
      sinks:
          - meter_sink
    - name: cpu_source
      meters:
          - "cpu"
      sinks:
          - cpu_sink
          - cpu_delta_sink
    - name: disk_source
      meters:
          - "disk.read.bytes"
          - "disk.read.requests"
          - "disk.write.bytes"
          - "disk.write.requests"
          - "disk.device.read.bytes"
          - "disk.device.read.requests"
          - "disk.device.write.bytes"
          - "disk.device.write.requests"
      sinks:
          - disk_sink
    - name: network_source
      meters:
          - "network.incoming.bytes"
          - "network.incoming.packets"
          - "network.outgoing.bytes"
          - "network.outgoing.packets"
      sinks:
          - network_sink
sinks:
    - name: meter_sink
      transformers:
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: cpu_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                target:
                    name: "cpu_util"
                    unit: "%"
                    type: "gauge"
                    max: 100
                    scale: "100.0 / (10**9 * (resource_metadata.cpu_number or 1))"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: cpu_delta_sink
      transformers:
          - name: "delta"
            parameters:
                target:
                    name: "cpu.delta"
                growth_only: True
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: disk_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                    map_from:
                        name: "(disk\\.device|disk)\\.(read|write)\\.(bytes|requests)"
                        unit: "(B|request)"
                target:
                    map_to:
                        name: "\\1.\\2.\\3.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high
    - name: network_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                   map_from:
                       name: "network\\.(incoming|outgoing)\\.(bytes|packets)"
                       unit: "(B|packet)"
                target:
                    map_to:
                        name: "network.\\1.\\2.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://?filter_project=service&archive_policy=high

EOT


sudo rm /etc/ceilometer/polling.yaml
sudo cat <<EOT>> /etc/ceilometer/polling.yaml
---
sources:
    - name: some_pollsters
      interval: 5
      meters:
        - cpu
        - cpu_l3_cache
        - memory.usage
        - network.incoming.bytes
        - network.incoming.packets
        - network.outgoing.bytes
        - network.outgoing.packets
        - disk.device.read.bytes
        - disk.device.read.requests
        - disk.device.write.bytes
        - disk.device.write.requests
        - hardware.cpu.util
        - hardware.memory.used
        - hardware.memory.total
        - hardware.memory.buffer
        - hardware.memory.cached
        - hardware.memory.swap.avail
        - hardware.memory.swap.total
        - hardware.system_stats.io.outgoing.blocks
        - hardware.system_stats.io.incoming.blocks
        - hardware.network.ip.incoming.datagrams
        - hardware.network.ip.outgoing.datagrams
EOT

systemctl enable openstack-ceilometer-compute.service
systemctl start openstack-ceilometer-compute.service
systemctl restart openstack-nova-compute.service

shutdown -r now

