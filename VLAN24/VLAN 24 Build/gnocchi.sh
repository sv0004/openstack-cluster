# Install base CentOS7 on the controller

# Configure hostsnames
sudo rm /etc/hostname
sudo cat <<EOT>> /etc/hostname
vlan24-os-gnocchi
EOT

# Update the host

sudo yum -y update
sudo yum -y upgrade
sudo yum -y install crudini
sudo yum -y install tmux

# Configure Host Files for DNS

sudo rm /etc/hosts
sudo cat <<EOT>> /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.5.24.31   vlan24-os-controller   vlan24-os-controller.unisurrey.net
10.5.24.42   vlan24-os-compute   vlan24-os-compute.unisurrey.net
10.5.24.28   vlan24-os-block   vlan24-os-block.unisurrey.net
10.5.24.29   vlan24-os-gnocchi   vlan24-os-gnocchi.unisurrey.net
10.5.24.40   vlan24-os-ODL   vlan24-os-ODL.unisurrey.net
EOT

# Configure NTP
yum install chrony -y

sudo rm /etc/chrony.conf
sudo cat <<EOT>> /etc/chrony.conf
server vlan24-os-controller iburst


# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates
# if its offset is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync
EOT


systemctl enable chronyd.service
systemctl start chronyd.service

# install OpenStack Queens Packages
yum install centos-release-openstack-queens -y
yum upgrade -y
yum install python-openstackclient -y
yum install openstack-selinux -y
yum install crudini -y

# Stop and disable firewall
systemctl stop firewalld
systemctl disable firewalld




# Create Openstack Admin File
sudo rm /root/admin_rc
sudo cat <<EOT>> /root/admin_rc
export OS_USERNAME=admin
export OS_PASSWORD=ADMIN_PASS
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

sudo rm /root/demo_rc
sudo cat <<EOT>> /root/demo_rc
export OS_USERNAME=demo
export OS_PASSWORD=demo
export OS_PROJECT_NAME=demo
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://10.5.24.31:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=password
EOT

yum install mariadb mariadb-server python2-PyMySQL -y

#configure SQL
sudo rm /etc/my.cnf.d/openstack.cnf
sudo cat <<EOT>> /etc/my.cnf.d/openstack.cnf
[mysqld]

bind-address = 10.5.24.29

default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 10000
collation-server = utf8_general_ci
character-set-server = utf8
EOT

systemctl enable mariadb.service
systemctl start mariadb.service

#secure mysql (Run This section Manually)
mysql_secure_installation

#create ceilometer user
. /root/admin_rc

openstack user create --domain Default --password ceilometer123 ceilometer
openstack role add --project service --user ceilometer admin
openstack user create --domain Default --password gnocchi123 gnocchi
openstack service create --name gnocchi --description "Metric Service" metric
openstack role add --project service --user gnocchi admin
openstack endpoint create --region RegionOne metric public http://10.5.24.29:8041
openstack endpoint create --region RegionOne metric internal http://10.5.24.29:8041
openstack endpoint create --region RegionOne metric admin http://10.5.24.29:8041

yum install openstack-gnocchi-api openstack-gnocchi-metricd python-gnocchiclient -y
yum install httpd mod_wsgi -y

yum install python-pip
yum groupinstall "Development Tools"
pip install uwsgi

#configure httpd

sudo cat <<EOT>> /etc/httpd/conf/httpd.conf
ServerName 10.5.24.29
EOT

systemctl enable httpd.service
systemctl start httpd.service

#configure sql

mysql -u root -pabc.123 -Bse "CREATE DATABASE gnocchi;"
mysql -u root -pabc.123 -Bse "GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'localhost' IDENTIFIED BY 'GNOCCHI_DBPASS';"
mysql -u root -pabc.123 -Bse "GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'%' IDENTIFIED BY 'GNOCCHI_DBPASS';"

crudini --set  /etc/gnocchi/gnocchi.conf api auth_mode "keystone"

crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken auth_type "password"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken auth_url "http://10.5.24.31:5000/v3"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken project_domain_name "Default"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken user_dmain_name "Default"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken project_name "service"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken username "gnocchi"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken password "gnocchi123"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken interface "internalURL"
crudini --set  /etc/gnocchi/gnocchi.conf keystone_authtoken region_name "RegionOne"

crudini --set  /etc/gnocchi/gnocchi.conf indexer url "mysql+pymysql://gnocchi:GNOCCHI_DBPASS@10.5.24.29/gnocchi"

#crudini --set  /etc/gnocchi/gnocchi.conf storage coordination_url "redis://10.5.24.31:6379"
crudini --set  /etc/gnocchi/gnocchi.conf storage file_basepath "/home/gnocchi"
crudini --set  /etc/gnocchi/gnocchi.conf storage driver "file"



gnocchi-upgrade

systemctl enable openstack-gnocchi-api.service openstack-gnocchi-metricd.service
systemctl start openstack-gnocchi-api.service openstack-gnocchi-metricd.service


# install and config ceilometer
yum -y install openstack-ceilometer-central openstack-ceilometer-notification python2-ceilometerclient 

sudo rm /etc/ceilometer/ceilometer.conf
sudo cat <<EOT>> /etc/ceilometer/ceilometer.conf
[DEFAULT]
http_timeout=600
debug=False
log_dir=/var/log/ceilometer
transport_url = rabbit://openstack:RABBIT_PASS@10.5.24.31
polling_namespaces=central,compute,ipmi
[compute]
[coordination]
backend_url=redis://127.0.0.1:6379
[dispatcher_gnocchi]
archive_policy = high
[event]
[hardware]
[ipmi]
[matchmaker_redis]
[meter]
[notification]
ack_on_event_error=True
[oslo_concurrency]
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
topics=notifications
[oslo_messaging_rabbit]
ssl=False
[oslo_messaging_zmq]
[polling]
[publisher]
[publisher_notifier]
[rgw_admin_credentials]
[service_credentials]
auth_type=password
auth_url=http://10.5.24.31:5000/
project_name=service
project_domain_name=default
username=ceilometer
user_domain_name=default
password=ceilometer123
region_name=RegionOne
[service_types]
[vmware]
[xenapi]
[database]
event_time_to_live=-1
metering_time_to_live=-1
[keystone_authtoken]
auth_uri=http://10.5.24.31:5000/v3
auth_type=password
auth_url=http://10.5.24.31:35357
username=ceilometer
user_domain_name=default
project_name=service
project_domain_name=default
password=ceilometer123
EOT

sudo rm /etc/ceilometer/pipeline.yaml
sudo cat <<EOT>> /etc/ceilometer/pipeline.yaml
---
sources:
    - name: meter_source
      meters:
          - "*"
      sinks:
          - meter_sink
    - name: cpu_source
      meters:
          - "cpu"
      sinks:
          - cpu_sink
          - cpu_delta_sink
    - name: disk_source
      meters:
          - "disk.read.bytes"
          - "disk.read.requests"
          - "disk.write.bytes"
          - "disk.write.requests"
          - "disk.device.read.bytes"
          - "disk.device.read.requests"
          - "disk.device.write.bytes"
          - "disk.device.write.requests"
      sinks:
          - disk_sink
    - name: network_source
      meters:
          - "network.incoming.bytes"
          - "network.incoming.packets"
          - "network.outgoing.bytes"
          - "network.outgoing.packets"
      sinks:
          - network_sink
sinks:
    - name: meter_sink
      transformers:
      publishers:
          - gnocchi://
          - notifier://?filter_project=service&archive_policy=high
    - name: cpu_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                target:
                    name: "cpu_util"
                    unit: "%"
                    type: "gauge"
                    max: 100
                    scale: "100.0 / (10**9 * (resource_metadata.cpu_number or 1))"
      publishers:
          - gnocchi://
          - notifier://?filter_project=service&archive_policy=high
    - name: cpu_delta_sink
      transformers:
          - name: "delta"
            parameters:
                target:
                    name: "cpu.delta"
                growth_only: True
      publishers:
          - gnocchi://
          - notifier://?filter_project=service&archive_policy=high
    - name: disk_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                    map_from:
                        name: "(disk\\.device|disk)\\.(read|write)\\.(bytes|requests)"
                        unit: "(B|request)"
                target:
                    map_to:
                        name: "\\1.\\2.\\3.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://
          - notifier://?filter_project=service&archive_policy=high
    - name: network_sink
      transformers:
          - name: "rate_of_change"
            parameters:
                source:
                   map_from:
                       name: "network\\.(incoming|outgoing)\\.(bytes|packets)"
                       unit: "(B|packet)"
                target:
                    map_to:
                        name: "network.\\1.\\2.rate"
                        unit: "\\1/s"
                    type: "gauge"
      publishers:
          - gnocchi://
          - notifier://?filter_project=service&archive_policy=high
EOT

sudo rm /etc/ceilometer/polling.yaml
sudo cat <<EOT>> /etc/ceilometer/polling.yaml
---
sources:
    - name: some_pollsters
      interval: 10
      meters:
        - cpu
        - cpu_l3_cache
        - memory.usage
        - network.incoming.bytes
        - network.incoming.packets
        - network.outgoing.bytes
        - network.outgoing.packets
        - disk.device.read.bytes
        - disk.device.read.requests
        - disk.device.write.bytes
        - disk.device.write.requests
        - hardware.cpu.util
        - hardware.memory.used
        - hardware.memory.total
        - hardware.memory.buffer
        - hardware.memory.cached
        - hardware.memory.swap.avail
        - hardware.memory.swap.total
        - hardware.system_stats.io.outgoing.blocks
        - hardware.system_stats.io.incoming.blocks
        - hardware.network.ip.incoming.datagrams
        - hardware.network.ip.outgoing.datagrams
EOT

#do ceilometer install
do ceilometer-upgrade --skip-metering-database

systemctl enable openstack-ceilometer-central.service openstack-ceilometer-notification.service
systemctl start openstack-ceilometer-central.service openstack-ceilometer-notification.service









